<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Advance SQL 4.1</title>
  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <br>
    <div class="container">
    <h1>Selecting by age using birth date</h1>
    <textarea name="name" rows="3" cols="150">SELECT first_name, last_name, birth_date, TIMESTAMPDIFF(YEAR, birth_date, CURDATE()) AS age FROM employees WHERE TIMESTAMPDIFF(YEAR, birth_date, CURDATE()) > 30 and TIMESTAMPDIFF(YEAR, birth_date, CURDATE()) < 40;</textarea>
    </div>
  </body>
</html>
