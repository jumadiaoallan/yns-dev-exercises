<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Advance SQL 4.2</title>
  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <div class="container mt-2">
    <h3>List all therapists according to their daily work shifts</h3>
    <textarea name="name" rows="20" cols="150">
A=>
CREATE TABLE IF NOT EXISTS therapists(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,name VARCHAR(100) NOT NULL,);
CREATE TABLE IF NOT EXISTS daily_work_shifts(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, therapist_id int not null, target_date date not null,start_time time not null,end_time time not null,);
B=>
INSERT INTO therapists VALUES('','John');
INSERT INTO therapists VALUES('','Arnold');
INSERT INTO therapists VALUES('','Robert');
INSERT INTO therapists VALUES('','Ervin');
INSERT INTO therapists VALUES('','Smith');
INSERT INTO daily_work_shifts VALUES('',1,NOW(),'14:00:00','15:00:00');
INSERT INTO daily_work_shifts VALUES('',2,NOW(),'22:00:00','23:00:00');
INSERT INTO daily_work_shifts VALUES('',3,NOW(),'00:00:00','01:00:00');
INSERT INTO daily_work_shifts VALUES('',4,NOW(),'5:00:00','5:30:00');
INSERT INTO daily_work_shifts VALUES('',1,NOW(),'21:00:00','21:45:00');
INSERT INTO daily_work_shifts VALUES('',5,NOW(),'5:30:00','5:50:00');
INSERT INTO daily_work_shifts VALUES('',3,NOW(),'2:00:00','2:30:00');
C=>
SELECT therapist_id,target_date,start_time,end_time, CASE WHEN start_time <= ' 05:59:59' AND start_time >= ' 00:00:00' THEN CONCAT(target_date + interval 1 day, ' ' , start_time) ELSE CONCAT(target_date , ' ' , start_time) END sort_start_time FROM daily_work_shifts INNER JOIN therapists on therapists.id = therapist_id ORDER BY target_date ASC, sort_start_time ASC;
    </textarea>
  </div>
  </body>
</html>
