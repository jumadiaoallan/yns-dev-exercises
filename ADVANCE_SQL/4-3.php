<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>ADVANCE SQL 4.3</title>
  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <br>
    <div class="container">
    <h3>Using Case conditional statement</h3>
    <textarea name="name" rows="3" cols="150">SELECT * , CASE WHEN positions.name = "CEO" THEN "Chief Executive Officer" WHEN positions.name = "CTO" THEN "Chief Technical Officer" WHEN positions.name = "CFO" THEN "Chief Financial Officer" ELSE positions.name END AS pos_name FROM employee_positions INNER JOIN employees ON employees.id = employee_positions.employee_id INNER JOIN positions ON positions.id = employee_positions.position_id;</textarea>
    </div>
  </body>
</html>
