<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Advance 4.5</title>
  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <div class="container mt-3">
    <h3>Create sql that will display the OUTPUT</h3>
    <textarea name="name" rows="3" cols="150">SELECT id, parent_id FROM ( SELECT id, parent_id , IF(parent_id IS NULL, @newOrder := IF(@newOrder + 1 IN (SELECT parent_id FROM execise_4_4 WHERE parent_id IS NOT NULL), @newOrder + 1, @newOrder + 2), NULL) new_order FROM execise_4_4, (SELECT @newOrder := 0) var_init_subquery_alias ORDER BY id ASC ) sq ORDER BY COALESCE(new_order, parent_id);</textarea>
    </div>
  </body>
</html>
