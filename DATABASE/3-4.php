<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title></title>
  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <br>
    <div class="container">
    <h1>SQL PROBLEM</h1>

  <label for="1">1) Create all the tables in your MySQL. Try to define primary key, data type, not null constraint.</label><br>
  <textarea id="1" name="name" rows="10" cols="150">CREATE TABLE `yns`.`employees` ( `id` INT NOT NULL AUTO_INCREMENT , `first_name` VARCHAR(255) NOT NULL , `last_name` VARCHAR(255) NOT NULL , `middle_name` VARCHAR(255) NOT NULL , `birth_date` DATE NOT NULL , `department_id` INT NOT NULL , `hire_date` DATE NOT NULL , `boss_id` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `yns`.`departments` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `yns`.`positions` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(100) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `yns`.`employee_positions` ( `id` INT NOT NULL AUTO_INCREMENT , `employee_id` INT NOT NULL , `position_id` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
</textarea> <br>
  <label for="2">2) Insert all the data into the tables that you have created.</label> <br>
  <textarea for="2" name="name" rows="8" cols="150">INSERT INTO `employees` (`id`, `first_name`, `last_name`, `middle_name`, `birth_date`, `department_id`, `hire_date`, `boss_id`) VALUES (NULL, 'Manabu', 'Yamazaki', '', '1976-03-15', '1', '', ''), (NULL, 'Tomohiko', 'Takasago', '', '1974-05-24', '3', '2014-04-01', '1');
INSERT INTO `employees` (`id`, `first_name`, `last_name`, `middle_name`, `birth_date`, `department_id`, `hire_date`, `boss_id`) VALUES (NULL, 'Yuta', 'Kawakami', NULL, '1990-08-13', '4', '2014-04-01', '1'), (NULL, 'Shogo', 'Kubota', NULL, '1985-01-31', '4', '2014-12-01', '1');
INSERT INTO `employees` (`id`, `first_name`, `last_name`, `middle_name`, `birth_date`, `department_id`, `hire_date`, `boss_id`) VALUES (NULL, 'Lorraine', 'San Jose', 'P.', '1983-10-11', '2', '2015-03-10', '1'), (NULL, 'Haille', 'Dela Cruz', 'A.', '1990-11-12', '3', '2015-02-15', '2');
INSERT INTO `employees` (`id`, `first_name`, `last_name`, `middle_name`, `birth_date`, `department_id`, `hire_date`, `boss_id`) VALUES (NULL, 'Godfrey', 'Sarmenta', 'L.', '1993-09-13', '4', '2015-01-01', '1'), (NULL, 'Alex', 'Amistad', 'F.', '1988-04-14', '4', '2015-04-10', '1');
INSERT INTO `employees` (`id`, `first_name`, `last_name`, `middle_name`, `birth_date`, `department_id`, `hire_date`, `boss_id`) VALUES (NULL, 'Hideshi', 'Ogoshi', NULL, '1983-07-15', '4', '2014-06-01', '1'), (NULL, 'Kim', '', NULL, '1977-10-16', '5', '2015-08-06', '1');
INSERT INTO `departments` (`id`, `name`) VALUES (NULL, 'Exective'), (NULL, 'Admin');
INSERT INTO `departments` (`id`, `name`) VALUES (NULL, 'Sales'), (NULL, 'Development');
INSERT INTO `departments` (`id`, `name`) VALUES (NULL, 'Design'), (NULL, 'Marketing');
INSERT INTO `positions` (`id`, `name`) VALUES (NULL, 'CEO'), (NULL, 'CTO');
INSERT INTO `positions` (`id`, `name`) VALUES (NULL, 'CFO'), (NULL, 'Manager');
INSERT INTO `positions` (`id`, `name`) VALUES (NULL, 'Staff');
INSERT INTO `employee_positions` (`id`, `employee_id`, `position_id`) VALUES (NULL, '1', '1'), (NULL, '1', '2'), (NULL, '1', '3'), (NULL, '2', '4'), (NULL, '3', '5'), (NULL, '4', '5'), (NULL, '5', '5'), (NULL, '6', '5'), (NULL, '7', '5'), (NULL, '8', '5'), (NULL, '9', '5'), (NULL, '10', '5');
  </textarea><br>
<label>* Exercise SELECT statement.</label> <br>
<label>1) Retrieve employees whose last name start with "K".</label> <br>
<textarea name="name" rows="3" cols="150">SELECT * FROM `employees` WHERE last_name LIKE 'k%';</textarea> <br>
<label>2) Retrieve employees whose last name end with "i".</label> <br>
<textarea name="name" rows="3" cols="150">SELECT * FROM `employees` WHERE last_name LIKE '%i';</textarea> <br>
<label>3) Retrieve employee's full name and their hire date whose hire date is between 2015/1/1 and 2015/3/31 ordered by ascending by hire date.</label> <br>
<textarea name="name" rows="3" cols="150">SELECT CONCAT(first_name, " ", middle_name, " ", last_name) AS full_name, hire_date FROM `employees` WHERE hire_date BETWEEN '2015-01-01' AND '2015-03-31' ORDER BY hire_date;</textarea> <br>
<label>4) Retrieve employee's last name and their boss's last name. If they don't have boss, no need to retrieve and show.</label> <br>
<textarea name="name" rows="3" cols="150">SELECT emp.last_name EMPLOYEE, boss.last_name BOSS from employees emp LEFT JOIN employees boss on emp.boss_id = boss.id WHERE emp.boss_id IS NOT NULL;</textarea> <br>
<label>5) Retrieve employee's last name who belong to Sales department ordered by descending by last name.</label> <br>
<textarea name="name" rows="3" cols="150">SELECT last_name FROM employees LEFT JOIN departments ON employees.department_id = departments.id where departments.name = "Sales";</textarea> <br>
<label>6) Retrieve number of employee who has middle name.</label> <br>
<textarea name="name" rows="3" cols="150">SELECT COUNT(*) count_has_middle FROM employees WHERE middle_name is not null AND middle_name != '';</textarea> <br>
<label>
7) Retrieve department name and number of employee in each department. You don't need to retrieve the department name which doesn't have employee.
</label> <br>
<textarea name="name" rows="3" cols="150">SELECT departments.name, COUNT(employees.id) FROM departments JOIN employees ON employees.department_id = departments.id GROUP BY departments.id;</textarea> <br>
<label>8) Retrieve employee's full name and hire date who was hired the most recently.</label> <br>
<textarea name="name" rows="3" cols="150">SELECT first_name, middle_name, last_name, hire_date FROM `employees` ORDER BY hire_date DESC LIMIT 1;</textarea> <br>
<label>9) Retrieve department name which has no employee.</label> <br>
<textarea name="name" rows="3" cols="150">SELECT name, id from departments WHERE NOT EXISTS (SELECT employees.department_id from employees WHERE departments.id = employees.department_id);</textarea> <br>
<label>10) Retrieve employee's full name who has more than 2 positions.</label> <br>
<textarea name="name" rows="3" cols="150">SELECT employees.first_name, employees.last_name FROM employees INNER JOIN employee_positions ON employee_positions.employee_id = employees.id GROUP BY employees.id HAVING COUNT(*) > 2;</textarea>
</div>

  </body>
</html>
