<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../../PRACTICE/style.css">
    <title>User Login</title>

    <?php
    if (isset($_SESSION['username'])) {
      echo "<script>window.location.href = 'users.php';</script>";
    }
     ?>


  </head>
  <body>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>

    <div class="container">
      <div class="card p-4 mt-5">
        <div class="alert text-warning">
          <?php if (isset($_SESSION['loginErr'])): ?>
            <?php echo $_SESSION['loginErr'] ?>
          <?php endif; ?>
        </div>
        <form class="" action="process.php" method="post">
          <div class="form-group row mt-2">
            <label for="uname" class="col-sm-2 col-form-label col-form-label-sm">Username/Email: </label>
            <div class="col-sm-10">
              <input type="email" class="form-control form-control-sm" id="uname" name="uname" value="" placeholder="Username/Email">
            </div>
          </div>
          <div class="form-group row mt-2">
            <label for="password" class="col-sm-2 col-form-label col-form-label-sm">Password: </label>
            <div class="col-sm-10">
              <input type="password" class="form-control form-control-sm" id="password" name="password" value="" placeholder="password">
            </div>
          </div>
          <div class="row mt-2">
            <div class="col-sm-12">
              <input type="submit" class="btn btn-primary btn-block float-end" id="submit" name="login" value="Login" >
            </div>
          </div>
        </form>
      </div>
  </div>
  </body>
</html>
