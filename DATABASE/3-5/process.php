<?php
include_once '../../DATABASE/3-5/config.php';
session_start();

if (isset($_POST["login"])) {

  $username = $_POST['uname'];
  $password = md5($_POST['password']);

   $query = "SELECT email, password FROM employees WHERE email = ? AND password = ?";

   if ($stmt = $mysqli->prepare($query)) {
     $stmt->bind_param("ss", $username, $password);

     if ($stmt->execute()) {
       $result = $stmt->get_result();
       if ($result->num_rows == 1) {

         $row = $result->fetch_array();

         $_SESSION['username'] = $row['email'];

         unset($_SESSION['loginErr']);
         echo "<script>window.location.href = 'users.php';</script>";
       } else {
         $_SESSION['loginErr'] = "Wrong Username/Email or Password!";
         echo "<script>window.location.href = 'login.php';</script>";
       }
     }

   }
 }

 if (isset($_POST["save"])) {

   $name = $lastname = $middlename = $hire = $bday = $email = $password = $path = $result = "";

   if (empty($_REQUEST['name'])) {
     $_SESSION['nameErr'] = "Missing Name";
   } else {
     if (!preg_match ("/^[a-zA-z]*$/", $_REQUEST['name'])) {
       $_SESSION['nameErr'] = "Name: Only alphabets and whitespace are allowed.";
     } else {
       unset($_SESSION['nameErr']);
     }
   }

   if (empty($_REQUEST['lastname'])) {
     $_SESSION['lastnameErr'] = "Missing Lastname";
   } else {
     if (!preg_match ("/^[a-zA-z]*$/", $_REQUEST['lastname'])) {
       $_SESSION['lastnameErr'] = "Lastname: Only alphabets and whitespace are allowed.";
     } else {
       unset($_SESSION['lastnameErr']);
     }
   }

   if (empty($_REQUEST['middlename'])) {
     if (!preg_match ("/^[a-zA-z]*$/", $_REQUEST['middlename'])) {
       $_SESSION['middlenameErr'] = "Middlename: Only alphabets and whitespace are allowed.";
     } else {
       unset($_SESSION['middlenameErr']);
     }
   }

   if (empty($_REQUEST['birth_date'])) {
     $_SESSION['birth_dateErr'] = "Missing Birthday";
   } else {
     unset($_SESSION['birth_dateErr']);
   }

   if (empty($_REQUEST['hire_date'])) {
     $_SESSION['hire_dateErr'] = "Missing Hire Date";
   } else {
     unset($_SESSION['hire_dateErr']);
   }

   $pattern = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^";

   if (empty($_REQUEST['email'])) {
     $_SESSION['email'] = "Missing Email";
   } else {
     if (!preg_match ($pattern, $_REQUEST['email'])) {
      $_SESSION['emailErr'] = "Email: Invalid Email.";
     } else {
       unset($_SESSION['emailErr']);
     }
   }

   $password = $_POST['password'];
   $number = preg_match('@[0-9]@', $password);
   $uppercase = preg_match('@[A-Z]@', $password);
   $lowercase = preg_match('@[a-z]@', $password);
   $specialChars = preg_match('@[^\w]@', $password);

   if(strlen($password) < 8 || !$number || !$uppercase || !$lowercase || !$specialChars) {
     $_SESSION['passwordErr'] = "Password must be at least 8 characters in length and must contain at least one number, one upper case letter, one lower case letter and one special character.";
   } else {
     unset($_SESSION['passwordErr']);
   }

   $count_Error = count($_SESSION);
   if ($count_Error > 1) {

     if(isset($_FILES["file"]) && $_FILES["file"]["error"] == 0){
         $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
         $filename = $_FILES["file"]["name"];
         $filetype = $_FILES["file"]["type"];
         $filesize = $_FILES["file"]["size"];
         $ext = pathinfo($filename, PATHINFO_EXTENSION);
         if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format.");
         $maxsize = 5 * 1024 * 1024;
         if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");

         if(in_array($filetype, $allowed)){
             if(file_exists("upload/" . $filename)){
                 $_SESSION["imageErr"] = $filename . " is already exists.";
             } else{
                 unset($_SESSION['imageErr']);
                 move_uploaded_file($_FILES["file"]["tmp_name"], "upload/" . $filename);
                 $path = "upload/" . $filename;
                 $result = "Your file was uploaded successfully.";
             }
         } else{
             $_SESSION["imageErr"] = "Error: There was a problem uploading your file. Please try again.";
         }
     } else{
         $_SESSION["imageErr"] = "Please select Image!";
     }

     if (empty($_SESSION["imageErr"])) {
        unset($_SESSION['imageErr']);

       $sql = "INSERT INTO employees (first_name, last_name, middle_name, birth_date, department_id, hire_date, boss_id, email, password, image) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

       if ($stmt = $mysqli->prepare($sql)) {
          $birt = date('Y-m-d', strtotime($_REQUEST['birth_date']));
          $stmt->bind_param('ssssisisss', $_REQUEST['name'], $_REQUEST['lastname'], $_REQUEST['middlename'], $birt, $_REQUEST['dept'], $_REQUEST['hire_date'], $_REQUEST['boss'],$_REQUEST['email'], md5($_POST['password']), $path);

          if ($stmt->execute()) {

            $last_id = $stmt->insert_id;
            $sql_position = "INSERT INTO employee_positions (employee_id, position_id) VALUES (?, ?)";
            if ($last_stmt = $mysqli->prepare($sql_position)) {
              $last_stmt->bind_param('ii', $last_id, $_REQUEST['pos']);
              if ($last_stmt->execute()) {
                $_SESSION['success'] = "Your inputted details was uploaded successfully.";
                echo "<script>window.location.href = 'users.php';</script>";
              }
            }
          } else {
            $_SESSION['Err'] = "Opps! Something went wrong. Please try again later";
          }

       }
     } else {

       echo "<script>window.location.href = 'users.php';</script>";
     }
   } else {
     if (empty($_FILES["file"])) {
       $_SESSION["imageErr"] = "Please select Image!";
     }
     echo "<script>window.location.href = 'users.php';</script>";
   }

 }

 ?>
