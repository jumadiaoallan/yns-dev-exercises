<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../../PRACTICE/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <title>User Information</title>
    <?php

    if (empty($_SESSION['username'])) {
      echo "<script>window.location.href = 'login.php';</script>";
    }

    if (isset($_GET['pageno'])) {
        $pageno = $_GET['pageno'];
    } else {
        $pageno = 1;
    }
    $no_of_records_per_page = 10;
    $offset = ($pageno-1) * $no_of_records_per_page;

     ?>

     <style media="screen">
       a {
         text-decoration: none;
       }
     </style>

  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <div class="container">
    <div class="alert text-warning">
      <?php echo isset($_SESSION['nameErr']) ? $_SESSION['nameErr'] : ""; ?>
      <?php echo isset($_SESSION['lastnameErr']) ? $_SESSION['lastnameErr'] :"" ; ?>
      <?php echo isset($_SESSION['middlenameErr']) ? $_SESSION['middlenameErr'] : "" ; ?>
      <?php echo isset($_SESSION['emailErr']) ? $_SESSION['emailErr'] : "" ; ?>
      <?php echo isset($_SESSION['passwordErr']) ? $_SESSION['passwordErr'] : "" ; ?>
      <?php echo isset($_SESSION['imageErr']) ? $_SESSION['imageErr'] : "" ; ?>
      <?php echo isset($_SESSION['hire_dateErr']) ? $_SESSION['hire_dateErr'] : "" ; ?>
      <?php echo isset($_SESSION['birth_dateErr']) ? $_SESSION['birth_dateErr'] : "" ; ?>
      <?php echo isset($_SESSION['Err']) ? $_SESSION['Err'] : "" ; ?>
      <?php
      unset($_SESSION['nameErr']);
      unset($_SESSION['lastnameErr']);
      unset($_SESSION['middlenameErr']);
      unset($_SESSION['emailErr']);
      unset($_SESSION['passwordErr']);
      unset($_SESSION['imageErr']);
      unset($_SESSION['hire_dateErr']);
      unset($_SESSION['birth_dateErr']);
      unset($_SESSION['Err']);
       ?>
    </div>
    <div class="alert text-success">
      <?php  echo isset($_SESSION['success']) ? $_SESSION['success'] : "" ; ?>
      <?php unset($_SESSION['success']); ?>
    </div>
    <div class="row">
      <div class="col-12">
        <?php if (isset($_SESSION['username'])): ?>
          <a href="logout.php" class="btn btn-danger float-end">Logout</a>
        <?php endif; ?>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <?php if (isset($_SESSION['username'])): ?>
          Hi, <?php echo $_SESSION['username']; ?>
        <?php endif; ?>
      </div>
    </div>
    <form class="mt-2" action="process.php" method="post"  enctype="multipart/form-data">
      <div class="form-group row mt-2">
        <label for="name" class="col-sm-2 col-form-label col-form-label-sm">Name: </label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="name" name="name" value="" placeholder="Name">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="lastname" class="col-sm-2 col-form-label col-form-label-sm">Lastname: </label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="lastname" name="lastname" value="" placeholder="Lastname">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="middlename" class="col-sm-2 col-form-label col-form-label-sm">Middlename: </label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="middlename" name="middlename" value="" placeholder="Middlename">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="bday" class="col-sm-2 col-form-label col-form-label-sm">Birthday: </label>
        <div class="col-sm-10">
          <input type="date" class="form-control form-control-sm" id="bday" name="birth_date" value="" placeholder="Birthday">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="email" class="col-sm-2 col-form-label col-form-label-sm">Email: </label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="name" name="email" value="" placeholder="Email">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="hire" class="col-sm-2 col-form-label col-form-label-sm">Hire Date: </label>
        <div class="col-sm-10">
          <input type="date" class="form-control form-control-sm" id="hire" name="hire_date" value="" placeholder="Hire Date">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="dept" class="col-sm-2 col-form-label col-form-label-sm">Department: </label>
        <div class="col-sm-10">
          <select class="form-control" name="dept">
            <option value="1">Executive</option>
            <option value="2">Admin</option>
            <option value="3">Sales</option>
            <option value="4">Development</option>
            <option value="5">Design</option>
            <option value="6">Marketing</option>
          </select>
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="pos" class="col-sm-2 col-form-label col-form-label-sm">Position: </label>
        <div class="col-sm-10">
          <select class="form-control" name="pos">
            <option value="1">CEO</option>
            <option value="2">CTO</option>
            <option value="3">CFO</option>
            <option value="4">Manager</option>
            <option value="5">Staff</option>
          </select>
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="boss" class="col-sm-2 col-form-label col-form-label-sm">Boss: </label>
        <div class="col-sm-10">
          <select class="form-control" name="boss">
            <option value="1">Yamazaki</option>
            <option value="2">Takasago</option>
          </select>
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="password" class="col-sm-2 col-form-label col-form-label-sm">Password: </label>
        <div class="col-sm-10">
          <input type="password" class="form-control form-control-sm" id="password" name="password" placeholder="Password">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="file" class="col-sm-2 col-form-label col-form-label-sm">Upload Image: </label>
        <div class="col-sm-10">
          <input type="file" class="form-control form-control-sm" id="file" name="file" placeholder="Image Upload">
        </div>
      </div>
      <div class="form-group row mt-2">
        <div class="col-sm-12">
          <input type="submit" class="form-control btn btn-primary btn-block" id="submit" name="save" value="Submit">
        </div>
      </div>
    </form>

    <div class="row mt-4">
      <h1 class="text-center">Employees List</h1>
      <div class="col-12 mt-2">
          <table class="table table-striped">
            <thead>
              <th>Firstname</th>
              <th>Lastname</th>
              <th>Middle Name</th>
              <th>Position</th>
              <th>Department</th>
              <th>Email</th>
              <th>Image</th>
            </thead>

            <tbody>
              <?php
                include_once 'config.php';

                $total_pages_sql = "SELECT count(*) FROM employees INNER JOIN departments ON departments.id = employees.department_id INNER JOIN employee_positions ON employees.id = employee_positions.employee_id LEFT JOIN positions ON positions.id = employee_positions.position_id";

                $count = $mysqli->query($total_pages_sql);
                $total_rows =  $count->fetch_array()[0];
                $total_pages = ceil($total_rows / $no_of_records_per_page);
                $select = "SELECT employees.first_name, employees.last_name, employees.middle_name, employees.email, employees.image, departments.name AS dept_name, positions.name AS pos_name FROM employees";
                $join = "INNER JOIN departments ON departments.id = employees.department_id INNER JOIN employee_positions ON employees.id = employee_positions.employee_id LEFT JOIN positions ON positions.id = employee_positions.position_id";
                $limit = "LIMIT $offset, $no_of_records_per_page";
                $sql = $select . " " . $join . " " . $limit;

                if ($result = $mysqli->query($sql)) {
                  if ($result->num_rows > 0) {
                    while ($row = $result->fetch_array()) {
                      echo "<tr>";
                      echo "<td>". $row['first_name'] ."</td>";
                      echo "<td>". $row['last_name'] ."</td>";
                      echo "<td>". $row['middle_name'] ."</td>";
                      echo "<td>". $row['pos_name'] ."</td>";
                      echo "<td>". $row['dept_name'] ."</td>";
                      echo "<td>". $row['email'] ."</td>";
                      echo "<td> <img src='"."$row[image]"."' height='100px'></td>";
                      echo "</tr>";

                    }
                  }
                }

               ?>

            </tbody>

          </table>
          <ul class="pagination">
            <li><a href="?pageno=1">First &nbsp;&nbsp;</a></li>
            <li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
                <a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">Prev &nbsp;&nbsp;&nbsp;</a>
            </li>
            <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
                <a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">Next &nbsp;&nbsp;&nbsp;</a>
            </li>
            <li><a href="?pageno=<?php echo $total_pages; ?>">Last </a></li>
        </ul>
      </div>
    </div>
      </div>
  </body>
</html>
