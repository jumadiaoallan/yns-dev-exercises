<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>4 Basic Operations Of Arithmetic </title>

    <?php
      $add = $sub = $mul = $div = "";
      $inputErr = "";

      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST['first_number']) || empty($_POST['second_number'])) {
          $inputErr = "Missing Input";
        } else {
          $add = $_POST['first_number'] + $_POST['second_number'];
          $sub = $_POST['first_number'] - $_POST['second_number'];
          $mul = $_POST['first_number'] * $_POST['second_number'];
          $div = $_POST['first_number'] / $_POST['second_number'];
        }
      }

    ?>

  </head>
  <body>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <br><br>
    <?php echo $inputErr;?>
    <form class="" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
      <label for="first_number">First No:</label>
      <input type="number" id="first_number" name="first_number" value=""/>
      <label for="second_number">Second No:</label>
      <input type="number" id="second_number" name="second_number" value=""/>
      <input type="submit" name="submit" value="Compute">
    </form>
    <br>
    <?php
    echo "Output:";
    echo "Addition: ". $add;
    echo "<br>";
    echo "Subtraction: ". $sub;
    echo "<br>";
    echo "Multiplication: ". $mul;
    echo "<br>";
    echo "Division: ". $div;
    ?>

  </body>
</html>
