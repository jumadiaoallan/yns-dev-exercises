<?php

  $inputErr = "";
  $gcd = "";
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST['first_number']) || empty($_POST['second_number'])) {
      $inputErr = "Missing Input";
    } else {

      $x = $_POST['first_number'];
      $y = $_POST['second_number'];

      if ($x > $y) {
        $temp = $x;
        $x = $y;
        $y = $temp;
      }

      for($i = 1; $i < ($x+1); $i++) {
        if ($x%$i == 0 and $y%$i == 0)
          $gcd = $i;
      }

    }
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Greatest Common Divisor</title>
</head>

<body>
      <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
      <br><br>
      <?php echo $inputErr;?>
      <form class="" action="<?php echo ($_SERVER["PHP_SELF"]);?>" method="post">
        <label for="first_number">First No:</label>
        <input type="number" id="first_number" name="first_number" value=""/>
        <label for="second_number">Second No:</label>
        <input type="number" id="second_number" name="second_number" value=""/>
        <input type="submit" name="submit" value="Compute">
      </form>
      <br>
        Output:
        <?php echo $gcd; ?>
</body>

</html>
