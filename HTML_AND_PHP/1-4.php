<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>FizzBuzz problem</title>
  </head>
  <?php

  $inputErr = "";

  if (empty($_POST['number'])) {
    $inputErr = "Missing Input";
  }

  ?>

  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <br><br>
    <?php echo $inputErr;?>
    <form class="" action="<?php echo ($_SERVER["PHP_SELF"]);?>" method="post">
      <input type="number" placeholder="Enter Number" name="number" value="">
      <input type="submit" name="submit" value="SUBMIT" />
    </form>
    Output: <br>
    <?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      if (!empty($_POST['number'])) {
        $input = $_POST['number'];
        for ($i = 1; $i <= $input; $i++) {
        $isFizz = ($i % 3 === 0);
        $isBuzz = ($i % 5 === 0);
        $isFizzBuzz = ($i % 15 === 0);

        if (!$isFizz && !$isBuzz) {
            echo $i . "<br>";
            continue;
        }
        if ($isFizz) {
            echo 'Fizz' . "<br>";
        }
        if ($isBuzz) {
              echo 'Buzz' . "<br>";
        }
        if ($isFizzBuzz) {
              echo 'FizzBuzz' . "<br>";
        }
        echo "";
        }
      }
    }

     ?>
  </body>
</html>
