<!DOCTYPE html>
<html lang="en">
<head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="../PRACTICE/nav.js" charset="utf-8"></script>
  <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Days from the input date</title>
</head>

<?php
  $inputErr = "";
  $data= "";
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST['date'])) {
      $inputErr = "Missing Input";
    } else {
      $date = $_POST['date'];
      $dateFormatted = date('F d, Y', strtotime($date));
      $dateEnd = date('F d, Y', strtotime($date . " + 3 days"));
    }
  }
?>

<body>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <br><br>
    <?php echo $inputErr;?>
    <form class="" action="<?php echo ($_SERVER["PHP_SELF"]);?>" method="post">
      <label for="date">Input Date:</label>
      <input name="date" type="date" value="">
      <input type="submit" name="submit" value="SUBMIT">
    </form>
        <br>Output:<br>
        <?php
        echo "From: ". date('F d, Y', strtotime($date)) . "<br>";
        if (isset($dateFormatted) && isset($dateEnd)) {
            while ($dateFormatted < $dateEnd) {
                $dateFormatted =  date('F d, Y', strtotime($dateFormatted . " + 1 days"));
                echo $dateFormatted . ' ' . date('D', strtotime($dateFormatted)), '<br>';
            }
        }
        ?>
</body>

</html>
