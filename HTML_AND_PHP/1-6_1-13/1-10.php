<?php session_start(); ?>
<!DOCTYPE html>
<?php
    if (empty($_SESSION['username'])) {
      echo "<script>window.location.href = '1-13.php';</script>";
    }
      $name = $lastname = $email = $age = $password = $path = $result = "";
      $nameErr = $lastnameErr = $emailErr = $ageErr = $passwordErr = $resultErr = "";

      if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (empty($_REQUEST['name'])) {
          $nameErr = "Missing Name";
        } else {
          if (!preg_match ("/^[a-zA-z]*$/", $_REQUEST['name'])) {
            $nameErr = "Name: Only alphabets and whitespace are allowed.";
          } else {
            $name = $_REQUEST['name'];
          }
        }

        if (empty($_REQUEST['lastname'])) {
          $lastnameErr = "Missing Lastname";
        } else {
          if (!preg_match ("/^[a-zA-z]*$/", $_REQUEST['lastname'])) {
            $lastnameErr = "Lastname: Only alphabets and whitespace are allowed.";
          } else {
            $lastname = $_REQUEST['lastname'];
          }
        }

        $pattern = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^";

        if (empty($_REQUEST['email'])) {
          $emailErr = "Missing email";
        } else {
          if (!preg_match ($pattern, $_REQUEST['email'])) {
            $emailErr = "Email: Invalid Email.";
          } else {
            $email = $_REQUEST['email'];
          }
        }

        if (empty($_REQUEST['age'])) {
          $ageErr = "Missing Age";
        } else {
            if (!preg_match ("/^[0-9]*$/", $_REQUEST['age'])) {
            $ageErr = "Age: Number Format Only.";
          } else {
            $age = $_REQUEST['age'];
          }
        }

        $password = $_POST['password'];
        $number = preg_match('@[0-9]@', $password);
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $specialChars = preg_match('@[^\w]@', $password);

        if(strlen($password) < 8 || !$number || !$uppercase || !$lowercase || !$specialChars) {
          $passwordErr = "Password must be at least 8 characters in length and must contain at least one number, one upper case letter, one lower case letter and one special character.";
        } else {
          $password = md5($_POST['password']);
        }

        if (empty($nameErr) && empty($lastnameErr) && empty($emailErr) && empty($ageErr) && empty($passwordErr)) {

          if(isset($_FILES["file"]) && $_FILES["file"]["error"] == 0){
              $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
              $filename = $_FILES["file"]["name"];
              $filetype = $_FILES["file"]["type"];
              $filesize = $_FILES["file"]["size"];
              $ext = pathinfo($filename, PATHINFO_EXTENSION);
              if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format.");
              $maxsize = 5 * 1024 * 1024;
              if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");

              if(in_array($filetype, $allowed)){
                  if(file_exists("upload/" . $filename)){
                      $resultErr = $filename . " is already exists.";
                  } else{
                      move_uploaded_file($_FILES["file"]["tmp_name"], "upload/" . $filename);
                      $path = "upload/" . $filename;
                      $result = "Your file was uploaded successfully.";
                  }
              } else{
                  $resultErr = "Error: There was a problem uploading your file. Please try again.";
              }
          } else{
              $resultErr = "Please select Image!";
          }

          if (empty($resultErr)) {
            $file_open = fopen("data.csv", "a");
            $no_rows = count(file("data.csv"));
            if($no_rows > 1)
            {
             $no_rows = ($no_rows - 1) + 1;
            }
            $form_data = array(
             $no_rows,
             $name,
             $lastname,
             $email,
             $age,
             $password,
             $path
            );
            fputcsv($file_open, $form_data);
            if (fclose($file_open)) {
                  $success = "All fields are inputted successfully!";
              }
          }
        }
      }
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../../PRACTICE/style.css">
    <title>User Information</title>
  </head>
  <body>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>

    <div class="container">
    <div class="mt-3 alert text-warning">
      <?php echo $nameErr . " " . $lastnameErr . " " . $emailErr . " " . $ageErr . " " . $passwordErr . " " . $resultErr?>
    </div>
    <div class="mt-2 alert text-success">
      <?php  if (isset($success)) { echo $success; } ?>
    </div>
    <div class="row">
      <div class="col-12">
        <?php if (isset($_SESSION['username'])): ?>
          <a href="logout.php" class="btn btn-danger float-end">Logout</a>
        <?php endif; ?>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <?php if (isset($_SESSION['username'])): ?>
          Hi, <?php echo $_SESSION['username']; ?>
        <?php endif; ?>
      </div>
    </div>
    <form class="mt-2" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post"  enctype="multipart/form-data">
      <div class="form-group row mt-2">
        <label for="name" class="col-sm-2 col-form-label col-form-label-sm">Name: </label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="name" name="name" value="<?php echo $name ?>" placeholder="Name">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="lastname" class="col-sm-2 col-form-label col-form-label-sm">Lastname: </label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="lastname" name="lastname" value="<?php echo $lastname ?>" placeholder="Lastname">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="email" class="col-sm-2 col-form-label col-form-label-sm">Email: </label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="name" name="email" value="<?php echo $email ?>" placeholder="Email">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="age" class="col-sm-2 col-form-label col-form-label-sm">Age: </label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="age" name="age" value="<?php echo $age ?>" placeholder="Age">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="age" class="col-sm-2 col-form-label col-form-label-sm">Password: </label>
        <div class="col-sm-10">
          <input type="password" class="form-control form-control-sm" id="password" name="password" placeholder="Password">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="file" class="col-sm-2 col-form-label col-form-label-sm">Upload Image: </label>
        <div class="col-sm-10">
          <input type="file" class="form-control form-control-sm" id="file" name="file" placeholder="Image Upload">
        </div>
      </div>
      <div class="form-group row mt-2">
        <div class="col-sm-12">
          <input type="submit" class="form-control btn btn-primary btn-block" id="submit" name="submit" value="Submit">
        </div>
      </div>
    </form>
    <div class="form-group row mt-2">
      <div class="col-sm-12">
        <a href="1-12.php" class="form-control btn btn-warning btn-block">View List</a>
      </div>
    </div>
    </div>
  </body>
</html>
