<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../../PRACTICE/style.css">
    <title>User Information List</title>
  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <div class="container mt-3">
      <table class="table table-striped">
        <thead class="thead-dark">
          <tr>
            <th>No.</th>
            <th>Name</th>
            <th>Lastname</th>
            <th>Email</th>
            <th>Age</th>
            <th>Image</th>
          </tr>
          </thead>
          <tbody>
            <?php

            if (file_exists("data.csv")) {

              $file = fopen("data.csv", "r");
                while (($data = fgetcsv($file)) !== false) {

                    if (!empty($data)) {
                      if (!empty($data[6])) {
                        $path = $data[6];
                      } else {
                        $path = "default.jpg";
                      }
                      echo "<tr>
                            <td>$data[0]</td>
                            <td>$data[1]</td>
                            <td>$data[2]</td>
                            <td>$data[3]</td>
                            <td>$data[4]</td>
                            <td><img src='$path' height='100px' alt='$data[1]'></td>
                            </tr>";
                    }
                }
                fclose($file);
            } else {
              echo "<tr><td colspan='6' class='text-center'>CSV FILE IS MISSING...</tr>";
            }
              ?>
          </tbody>
      </table>
    </div>

  </body>
</html>
