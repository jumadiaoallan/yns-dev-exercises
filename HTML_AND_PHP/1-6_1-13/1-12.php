<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../../PRACTICE/style.css">
    <title>User Information List</title>

    <?php
    if (empty($_SESSION['username'])) {
      echo "<script>window.location.href = '1-13.php';</script>";
    }


    $array = [];
    $csv_to_array = [];
    $file = fopen("data.csv", "r");
    while (($line = fgetcsv($file)) !== false){
    	array_push($csv_to_array, $line);
    }
    fclose($file);
    $current_page = $_GET['page'] ?? 1;
    $total = count($csv_to_array);
    $page = ceil($total / 10);
    $array = array_slice($csv_to_array,($current_page - 1) * 10 ,10);

     ?>

  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <div class="container mt-3">
      <div class="row">
        <div class="col-12">
          <?php if (isset($_SESSION['username'])): ?>
            <a href="logout.php" class="btn btn-danger float-end">Logout</a>
          <?php endif; ?>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <?php if (isset($_SESSION['username'])): ?>
            Hi, <?php echo $_SESSION['username']; ?>
          <?php endif; ?>
        </div>
      <table class="table table-striped">
        <thead class="thead-dark">
          <tr>
            <th>No.</th>
            <th>Name</th>
            <th>Lastname</th>
            <th>Email</th>
            <th>Age</th>
            <th>Image</th>
          </tr>
          </thead>
          <tbody>
      			<?php $i=0; ?>
      			<?php  if(isset($array)):  ?>
      				<?php foreach ($array as $key => $data): ?>
      					<?php
                echo "<tr>
                      <td>$data[0]</td>
                      <td>$data[1]</td>
                      <td>$data[2]</td>
                      <td>$data[3]</td>
                      <td>$data[4]</td>
                      <td><img src='$data[6]' height='100px'></td>
                      </tr>";
                 ?>
      				<?php endforeach;  endif; ?>

      			</tbody>
      </table>
      <div class="text-center mb-5">
        Page:
        <?php for ($i=1 ; $i<=$page; $i++) echo '<a href="1-12.php?page='.$i.'" class="btn btn-default btn-sm"> '.$i.'</a>' ?>
      </div>
    </div>

  </body>
</html>
