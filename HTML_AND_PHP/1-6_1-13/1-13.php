<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../../PRACTICE/style.css">
    <title>User Login</title>

    <?php
    if (isset($_SESSION['username'])) {
      echo "<script>window.location.href = '1-10.php';</script>";
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $username = $_POST['uname'];
      $password = md5($_POST['password']);
      $file = fopen("data.csv", "r");

      while (($find = fgetcsv($file)) !== false) {
        if ($username == $find[3] && $password == $find[5]) {
          $_SESSION['username'] = $username;
				  echo "<script>window.location.href = '1-10.php';</script>";
        }
      }
      fclose($file);
      $loginErr = "Wrong username/email or password!";
    }


     ?>


  </head>
  <body>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>

    <div class="container">
      <div class="card p-4 mt-3">
        <div class="alert text-warning">
          <?php if (isset($loginErr)): ?>
            <?php echo $loginErr ?>
          <?php endif; ?>
        </div>
        <form class="" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
          <div class="form-group row mt-2">
            <label for="uname" class="col-sm-2 col-form-label col-form-label-sm">Username/Email: </label>
            <div class="col-sm-10">
              <input type="email" class="form-control form-control-sm" id="uname" name="uname" value="" placeholder="Username/Email">
            </div>
          </div>
          <div class="form-group row mt-2">
            <label for="password" class="col-sm-2 col-form-label col-form-label-sm">Password: </label>
            <div class="col-sm-10">
              <input type="password" class="form-control form-control-sm" id="password" name="password" value="" placeholder="password">
            </div>
          </div>
          <div class="row mt-2">
            <div class="col-sm-12">
              <input type="submit" class="btn btn-primary btn-block float-end" id="submit" name="submit" value="Login" >
            </div>
          </div>
        </form>
      </div>
  </div>
  </body>
</html>
