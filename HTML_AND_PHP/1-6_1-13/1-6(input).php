<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../../PRACTICE/style.css">
    <title>User Information (Input)</title>
  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <div class="container">


    <form class="mt-5" action="1-6(output).php" method="post">
      <div class="form-group row mt-2">
        <label for="name" class="col-sm-2 col-form-label col-form-label-sm">Name</label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="name" name="name" placeholder="Name">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="lastname" class="col-sm-2 col-form-label col-form-label-sm">Lastname</label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="lastname" name="lastname" placeholder="Lastname">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="email" class="col-sm-2 col-form-label col-form-label-sm">Email</label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="name" name="email" placeholder="Email">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="age" class="col-sm-2 col-form-label col-form-label-sm">Age</label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="age" name="age" placeholder="Age">
        </div>
      </div>
      <div class="form-group row mt-2">
        <div class="col-sm-12">
          <input type="submit" class="form-control btn btn-primary btn-block" id="submit" name="submit" value="Submit">
        </div>
      </div>
    </form>
    </div>
  </body>
</html>
