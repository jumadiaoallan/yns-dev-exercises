<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../../PRACTICE/style.css">
    <title>User Information</title>

    <?php
      $name = $lastname = $email = $age = "";
      $nameErr = $lastnameErr = $emailErr = $ageErr = "";

      if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (empty($_REQUEST['name'])) {
          $nameErr = "Missing Name";
        } else {
          if (!preg_match ("/^[a-zA-z]*$/", $_REQUEST['name'])) {
            $nameErr = "Name: Only alphabets and whitespace are allowed.";
          } else {
            $name = $_REQUEST['name'];
          }
        }

        if (empty($_REQUEST['lastname'])) {
          $lastnameErr = "Missing Lastname";
        } else {
          if (!preg_match ("/^[a-zA-z]*$/", $_REQUEST['lastname'])) {
            $lastnameErr = "Lastname: Only alphabets and whitespace are allowed.";
          } else {
            $lastname = $_REQUEST['lastname'];
          }
        }

        $pattern = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^";

        if (empty($_REQUEST['email'])) {
          $emailErr = "Missing email";
        } else {
          if (!preg_match ($pattern, $_REQUEST['email'])) {
            $emailErr = "Email: Invalid Email.";
          } else {
            $email = $_REQUEST['email'];
          }
        }

        if (empty($_REQUEST['age'])) {
          $ageErr = "Missing Age";
        } else {
            if (!preg_match ("/^[0-9]*$/", $_REQUEST['age'])) {
            $ageErr = "Age: Number Format Only.";
          } else {
            $age = $_REQUEST['age'];
          }
        }


      }
     ?>

  </head>
  <body>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>

    <div class="container">
    <div class="mt-5 alert text-warning">
      <?php echo $nameErr . " " . $lastnameErr . " " . $emailErr . " " . $ageErr ?>
    </div>
    <form class="mt-2" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
      <div class="form-group row mt-2">
        <label for="name" class="col-sm-2 col-form-label col-form-label-sm">Name: </label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="name" name="name" value="<?php echo $name ?>" placeholder="Name">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="lastname" class="col-sm-2 col-form-label col-form-label-sm">Lastname: </label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="lastname" name="lastname" value="<?php echo $lastname ?>" placeholder="Lastname">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="email" class="col-sm-2 col-form-label col-form-label-sm">Email: </label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="name" name="email" value="<?php echo $email ?>" placeholder="Email">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label for="age" class="col-sm-2 col-form-label col-form-label-sm">Age: </label>
        <div class="col-sm-10">
          <input type="text" class="form-control form-control-sm" id="age" name="age" value="<?php echo $age ?>" placeholder="Age">
        </div>
      </div>
      <div class="form-group row mt-2">
        <div class="col-sm-12">
          <input type="submit" class="form-control btn btn-primary btn-block" id="submit" name="submit" value="Submit">
        </div>
      </div>
    </form>
    </div>
  </body>
</html>
