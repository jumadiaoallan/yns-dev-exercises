<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Scroll up and down</title>
  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <div style="height: 1800px;background-color: powderblue">

        <button type="button" name="button" style="position: absolute;top: 100px;" onclick="down();" id="scrolldown">Scroll Down</button>

        <button type="button" name="button" style="position: absolute;bottom: -1140px;" onclick="up();" id="scrollup">Scroll Up</button>

    </div>
    <script type="text/javascript">
      function down() {
        document.getElementById("scrollup").scrollIntoView();
      }

      function up() {
        document.getElementById("scrolldown").scrollIntoView();
      }
    </script>

  </body>
</html>
