<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Background Change Color</title>

    <style media="screen">
    body {
        background: darkblue; /* Initial background */
        transition: background .5s; /* .5s how long transitions shoould take */
      }
    </style>

  </head>
  <body onload="changeColor();">
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
  </body>

  <script type="text/javascript">
    function changeColor() {
      var colors = [' lightblue', 'darkblue'];
      var active = 0;
      setInterval(function(){
          document.querySelector('body').style.background = colors[active];
          active++;
          if (active == colors.length) active = 0;
      }, 2000);
    }
  </script>

</html>
