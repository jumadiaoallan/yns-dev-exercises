<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Mouse Over and Out</title>
  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <br>
    <img src="/JAVASCRIPT/image/tiger-jpg.jpg" id="img" onmouseover="changeOver()" onmouseout="changeOut()" alt="">
  </body>

  <script type="text/javascript">

      function changeOver() {
        document.getElementById("img").src = "/JAVASCRIPT/image/kitten.png";
      }
      function changeOut() {
        document.getElementById("img").src = "/JAVASCRIPT/image/tiger-jpg.jpg";
      }
  </script>

</html>
