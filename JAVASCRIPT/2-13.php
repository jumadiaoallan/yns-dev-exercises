<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Change Size</title>
  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <br>

    <center>
    <img src="/JAVASCRIPT/image/kittens.png" id="img" alt=""> <br>
    <button type="button" onclick="changeSize(200)" name="button">Small</button>
    <button type="button" onclick="changeSize(350)" name="button">Medium</button>
    <button type="button" onclick="changeSize(500)" name="button">Large</button>
    </center>
  </body>

  <script type="text/javascript">
    function changeSize(size) {
      console.log(size);
      document.getElementById('img').height = size;
    }
  </script>

</html>
