<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Image In Drop Down</title>
  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <br>
    <center>
    <img src="/JAVASCRIPT/image/1.jpg" style="height:500px;" id="img" alt=""> <br>
    <select id = "select_img" onchange="changeImage()">
      <option value="1.jpg">Image - 1</option>
      <option value="2.jpg">Image - 2</option>
      <option value="3.jpg">Image - 3</option>
      <option value="4.jpg">Image - 4</option>
      <option value="5.jpg">Image - 5</option>
    </select>
    </center>
  </body>

  <script type="text/javascript">
    function changeImage() {
      var img = document.getElementById('select_img').value;
      document.getElementById("img").src = "/JAVASCRIPT/image/"+img;
    }
  </script>

</html>
