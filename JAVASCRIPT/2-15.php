<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Date and Time</title>

    <script type="text/javascript">
      setInterval(function(){
            var d = new Date();
            document.getElementById("date_time").innerHTML = new Date(d.getTime()).toLocaleString('en-US');
          }, 60000);
    </script>

  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <br>
    <center><label id="date_time">  </label></center>
  </body>

  <script type="text/javascript">
  var dt = new Date();
  document.getElementById("date_time").innerHTML = new Date(dt.getTime()).toLocaleString('en-US');
  </script>

</html>
