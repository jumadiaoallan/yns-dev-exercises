<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Four basic operations of Arithmetic</title>
  </head>
  <body>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <br><br>
    <label for="first_number">First No:</label>
    <input type="number" id="first_number" name="first_number" value=""/>
    <label for="second_number">Second No:</label>
    <input type="number" id="second_number" name="second_number" value=""/>
    <select class="" name="operator" id = "operator">
      <option value="Addition">Addition</option>
      <option value="Subtraction">Subtraction</option>
      <option value="Multiplication">Multiplication</option>
      <option value="Division">Division</option>
    </select>
    <input type="submit" name="submit" value="Compute" onclick="compute();">
    <br>
    Result: <span id="result"></span>
  </body>

  <script type="text/javascript">
    function compute() {
      var first_number = document.getElementById('first_number').value;
      var second_number = document.getElementById('second_number').value;
      var operator = document.getElementById('operator').value;
      var result = 0;

      if (first_number == "" || second_number == "") {
        document.getElementById('result').innerHTML = "Input number to compute!";
      } else {
        if (operator == "Addition") {
          result = parseInt(first_number) + parseInt(second_number);
        } else if (operator == "Subtraction") {
          result = parseInt(first_number) - parseInt(second_number);
        } else if (operator == "Multiplication") {
          result = parseInt(first_number) * parseInt(second_number);
        } else if (operator == "Division") {
          result = parseInt(first_number) / parseInt(second_number);
        }
        document.getElementById('result').innerHTML = result;
      }


    }
  </script>


</html>
