<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Prime numbers</title>
  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <br><br>
    <input type="number" id="prime" value="" />
    <button type="button" name="button" onclick="prime();">Submit</button>
    <br>
    Result: <br>
    <span id="result"></span>
  </body>

  <script type="text/javascript">

    function prime() {
      var input = document.getElementById('prime').value;
      var numbers = [], primes = [], maxNumber = input;
      if (input <= 1) {
        document.getElementById('result').innerHTML = "Negative Number and 1 is not Prime Number";
      } else if (input == "") {
        document.getElementById('result').innerHTML = "Please input positive number";
      } else {
        for(var i = 2;i<=maxNumber;i++){
         numbers.push(i);
        }

        while(numbers.length){
            primes.push(numbers.shift());
            numbers = numbers.filter(
                function(i){return i%primes[primes.length-1]!=0}
            );
        }

        document.getElementById('result').innerHTML = primes;
      }

    }

  </script>


</html>
