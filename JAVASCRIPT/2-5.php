<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Typing copy</title>
  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <br>
    <input type="text" id="text" oninput="inputType();" />
    <br>
    Result: <br>
    <label id="result"></label>
  </body>

  <script type="text/javascript">

    function inputType() {
      var input = document.getElementById("text").value;
      document.getElementById('result').innerHTML = input;
    }

  </script>

</html>
