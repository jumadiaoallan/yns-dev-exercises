<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Add Label</title>
  </head>
  <body>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>

    <br><br>
    <button type="button" name="button" onclick="createdDOMLabel();">Add Label</button>
    <div id="result">

    </div>
  </body>

  <script type="text/javascript">
    function createdDOMLabel() {
      var createLabel = document.createElement("label");
      var linebreak = document.createElement('br');
      createLabel.innerHTML = "Created Label";
      document.getElementById("result").appendChild(linebreak);
      document.getElementById("result").appendChild(createLabel);
    }
  </script>

</html>
