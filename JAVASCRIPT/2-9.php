<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../PRACTICE/style.css">
    <title>Change Color</title>
  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <br>
    <button type="button" id="red" name="button" onclick="changeColor(this);"> RED </button>
    <button type="button" id="green" name="button" onclick="changeColor(this);">GREEN</button>
    <button type="button" id="blue" name="button"  onclick="changeColor(this);">BLUE</button>
    <button type="button" id="yellow" name="button" onclick="changeColor(this);">YELLOW</button>
    <button type="button" id="pink" name="button" onclick="changeColor(this);">PINK</button>
    <button type="button" id="orange" name="button" onclick="changeColor(this);">ORANGE</button>
  </body>

  <script type="text/javascript">
    function changeColor(color) {
      document.getElementById("red").style.backgroundColor = color.innerHTML;
      document.getElementById("green").style.backgroundColor = color.innerHTML;
      document.getElementById("blue").style.backgroundColor = color.innerHTML;
      document.getElementById("yellow").style.backgroundColor = color.innerHTML;
      document.getElementById("pink").style.backgroundColor = color.innerHTML;
      document.getElementById("orange").style.backgroundColor = color.innerHTML;
    }
  </script>

</html>
