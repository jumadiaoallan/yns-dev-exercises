<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../../PRACTICE/style.css">

    <title>Quiz Dashboard</title>

<?php
    if (isset($_GET['pageno'])) {
        $pageno = $_GET['pageno'];
    } else {
        $pageno = 1;
    }
    $no_of_records_per_page = 5;
    $offset = ($pageno-1) * $no_of_records_per_page;
 ?>

 <style media="screen">
   a {
     text-decoration: none;
   }
 </style>

  </head>
  <body>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <div class="container">
        <div style="margin-top:2%;" class="d-flex justify-content-center card p-3">
          <div class="text-center">
            <h3 class="card-title ">Add Question</h3>
          </div>
          <div class="panel panel-info" >
              <div class="panel-body p-3" >
                  <?php if (isset($_SESSION['questionErr']) || isset($_SESSION['aErr']) || isset($_SESSION['bErr']) || isset($_SESSION['cErr']) || isset($_SESSION['answerErr'])): ?>
                    <div class="alert alert-danger col-sm-12">
                      <?php echo $_SESSION['questionErr']." ".$_SESSION['aErr']." ".$_SESSION['bErr']." ".$_SESSION['cErr']." ".$_SESSION['answerErr']; ?>
                      <?php unset($_SESSION['aErr']) ?>
                      <?php unset($_SESSION['bErr']) ?>
                      <?php unset($_SESSION['cErr']) ?>
                      <?php unset($_SESSION['answerErr']) ?>
                    </div>
                  <?php endif; ?>
                  <?php if (isset($_SESSION['success'])): ?>
                    <div class="alert alert-success col-sm-12">
                      <?php echo $_SESSION['success']; ?>
                      <?php unset($_SESSION['success']) ?>
                    </div>
                  <?php endif; ?>
                  <form class="form-horizontal" method="POST" action="process.php">
                    <div class="form-group row">
                      <label for="question" class="col-sm-2 col-form-label">Question: </label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control d-none" name="id" id="id">
                        <input type="text" class="form-control" name="question" id="question">
                      </div>
                    </div>
                    <div class="form-group row mt-2">
                      <label for="question" class="col-sm-2 col-form-label">Choices: </label>
                      <div class="col-sm-2">
                        A. <input type="text" class="form-control" name="a" id="a" value="">
                      </div>
                      <div class="col-sm-3">
                        B. <input type="text" class="form-control" name="b" id="b" value="">
                      </div>
                      <div class="col-sm-3">
                        C. <input type="text" class="form-control" name="c" id="c" value="">
                      </div>
                      <div class="col-sm-2">
                        Answer:
                        <input type="text" class="form-control" name="answer" id="answer" value="">
                      </div>
                    </div>
                    <div class="col-sm-12 form-group row mt-2 ">
                      <input type="submit" class="form-control btn btn-primary" name="save_quiz" id="save" value="SUBMIT">
                      <input type="submit" class="form-control btn btn-warning d-none" name="edit_quiz" id="edit" value="SUBMIT">
                      <a href="quiz.php" class="form-control btn btn-success mt-2">TAKE A QUIZ</a>
                    </div>
                    </form>
                  </div>
              </div>
        </div>

        <div class="mt-3 row">
          <div class="col-sm-12">

            <table class="table table-bordered">
              <tr>
                <th>Questions</th>
                <th>Answers</th>
                <th>Action</th>
              </tr>
              <?php
              include_once '../../PRACTICE/6-1/config.php';

              $total_pages_sql = "SELECT COUNT(*) FROM questions INNER JOIN correct_answers ON questions.id = correct_answers.question_id INNER JOIN multiple_choices ON correct_answers.question_id = multiple_choices.question_id";

              $count = $mysqli->query($total_pages_sql);
              $total_rows =  $count->fetch_array()[0];
              $total_pages = ceil($total_rows / $no_of_records_per_page);

              $sql = "SELECT questions.id as question_id, questions.question, multiple_choices.choice_a, multiple_choices.choice_b, multiple_choices.choice_c, correct_answers.answers FROM questions INNER JOIN correct_answers ON questions.id = correct_answers.question_id INNER JOIN multiple_choices ON correct_answers.question_id = multiple_choices.question_id LIMIT $offset, $no_of_records_per_page";

              if ($result = $mysqli->query($sql)) {
                if ($result->num_rows > 0) {
                  while ($row = $result->fetch_array()) {
                    echo "<tr>";
                    echo "<td>". $row['question'] ."</td>";
                    echo "<td>". $row['answers'] ."</td>";
                    echo "<td>
                            <center>
                            <button type='button' name='button' data-value='$row[question_id],$row[question],$row[answers],$row[choice_a],$row[choice_b],$row[choice_c]' class='btn btn-warning btn-sm' onclick='edit(this)'>Edit</button>
                            <button class='btn btn-danger btn-sm' data-id='$row[question_id]' onclick='remove(this)'>Remove</button>
                            </center>
                          </td>";
                    echo "</tr>";
                  }
                  $mysqli->close();
                }

              } else {
                die($mysqli->connect_error);
              }

               ?>
            </table>

            <div class="row">
              <div class="col-sm-12 ">
                <ul class="pagination float-end">
                  <li><a href="?pageno=1">First &nbsp;&nbsp;</a></li>
                  <li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
                      <a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">Prev &nbsp;&nbsp;&nbsp;</a>
                  </li>
                  <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
                      <a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">Next &nbsp;&nbsp;&nbsp;</a>
                  </li>
                  <li><a href="?pageno=<?php echo $total_pages; ?>">Last </a></li>
              </ul>
              </div>
            </div>
          </div>
        </div>
    </div>

  </body>

  <script type="text/javascript">
    function edit(value) {
      let data = $(value).data('value');
      const split_data = data.split(",");

      document.getElementById('id').value = split_data[0];
      document.getElementById('question').value = split_data[1];
      document.getElementById('a').value = split_data[3];
      document.getElementById('b').value = split_data[4];
      document.getElementById('c').value = split_data[5];
      document.getElementById('answer').value = split_data[2];

      $("#edit").removeClass("d-none");
      $("#save").addClass("d-none");
    }

    function remove(id) {
      var id = $(id).data('id');
      if (confirm('Really want to remove this item?')) {
          location.href = 'process.php?remove='+id;
        }
    }
  </script>


</html>
