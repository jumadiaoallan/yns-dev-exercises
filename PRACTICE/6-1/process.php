<?php
include_once '../../PRACTICE/6-1/config.php';
session_start();
if (isset($_POST['save_quiz'])) {

  $question = $a = $b = $c = $answer = "";

  if (empty($_REQUEST['question'])) {
    $_SESSION['questionErr'] = "Missing Question";
  } else {
    unset($_SESSION['questionErr']);
    $question = $_REQUEST['question'];
  }

  if (empty($_REQUEST['a'])) {
    $_SESSION['aErr'] = "Missing A Choices";
  } else {
    unset($_SESSION['aErr']);
    $a = $_REQUEST['a'];
  }

  if (empty($_REQUEST['b'])) {
    $_SESSION['bErr'] = "Missing B Choices";
  } else {
    unset($_SESSION['bErr']);
    $b = $_REQUEST['b'];
  }

  if (empty($_REQUEST['c'])) {
    $_SESSION['cErr'] = "Missing C Choices";
  } else {
    unset($_SESSION['cErr']);
    $c = $_REQUEST['c'];
  }

  if (empty($_REQUEST['answer'])) {
    $_SESSION['answerErr'] = "Missing Answer";
  } else {
    unset($_SESSION['answerErr']);
    $answer = $_REQUEST['answer'];
  }


  if (empty($_SESSION['questionErr']) && empty($_SESSION['aErr']) && empty($_SESSION['bErr']) && empty($_SESSION['cErr']) && $_REQUEST['answer'] ) {

    $sql = "INSERT INTO questions (question) VALUES (?)";

    if ($stmt = $mysqli->prepare($sql)) {
      $stmt->bind_param('s', $question);
      if ($stmt->execute()) {
        $question_id = $stmt->insert_id;
        if ($question_id) {
          $insert_choices_sql = "INSERT INTO multiple_choices (question_id, choice_a, choice_b, choice_c) VALUES (?, ?, ?, ?)";
          if ($stmt_multi = $mysqli->prepare($insert_choices_sql)) {
            $stmt_multi->bind_param('isss', $question_id, $a, $b, $c);
            if ($stmt_multi->execute()) {
              $insert_answer_sql = "INSERT INTO correct_answers (question_id, answers) VALUES (?, ?)";
              if ($stmt_answer = $mysqli->prepare($insert_answer_sql)) {
                $stmt_answer->bind_param('is', $question_id, $answer);
                if ($stmt_answer->execute()) {
                  $_SESSION['success'] = "Question Added";
                  echo "<script>window.location.href = 'index.php';</script>";
                }
              }
            }
          }
        }

      }
    }

  } else {
    echo "<script>window.location.href = 'index.php';</script>";
  }

}

if (isset($_POST['edit_quiz'])) {

  if (empty($_REQUEST['question'])) {
    $_SESSION['questionErr'] = "Missing Question";
  } else {
    unset($_SESSION['questionErr']);
    $question = $_REQUEST['question'];
  }

  if (empty($_REQUEST['a'])) {
    $_SESSION['aErr'] = "Missing A Choices";
  } else {
    unset($_SESSION['aErr']);
    $a = $_REQUEST['a'];
  }

  if (empty($_REQUEST['b'])) {
    $_SESSION['bErr'] = "Missing B Choices";
  } else {
    unset($_SESSION['bErr']);
    $b = $_REQUEST['b'];
  }

  if (empty($_REQUEST['c'])) {
    $_SESSION['cErr'] = "Missing C Choices";
  } else {
    unset($_SESSION['cErr']);
    $c = $_REQUEST['c'];
  }

  if (empty($_REQUEST['answer'])) {
    $_SESSION['answerErr'] = "Missing Answer";
  } else {
    unset($_SESSION['answerErr']);
    $answer = $_REQUEST['answer'];
  }

  $id = $_REQUEST['id'];

  if (empty($_SESSION['questionErr']) && empty($_SESSION['aErr']) && empty($_SESSION['bErr']) && empty($_SESSION['cErr']) && $_REQUEST['answer'] ) {
        $sql = "UPDATE questions SET question = ? WHERE id = ?";

        if ($stmt = $mysqli->prepare($sql)) {
          $stmt->bind_param('si', $question, $id);
          if ($stmt->execute()) {
            $update_multi_choice = "UPDATE multiple_choices SET choice_a = ?, choice_b = ?, choice_c = ? WHERE question_id = ?";
            if ($stmt_multi = $mysqli->prepare($update_multi_choice)) {
              $stmt_multi->bind_param('sssi', $a, $b, $c, $id);
              if ($stmt_multi->execute()) {
                $update_answer = "UPDATE correct_answers SET answers = ? WHERE question_id = ?";
                if ($stmt_answer = $mysqli->prepare($update_answer)) {
                  $stmt_answer->bind_param('si', $answer, $id);
                  if ($stmt_answer->execute()) {
                    $_SESSION['success'] = "Question Added";
                    echo "<script>window.location.href = 'index.php';</script>";
                  }
                }
              }
            }
          }
        }
        $mysqli->close();
  } else {
    echo "<script>window.location.href = 'index.php';</script>";
  }

}

if (isset($_GET['remove'])) {

  $id = $_GET['remove'];

  $sql = "DELETE FROM questions WHERE id = ?";
  $multi = "DELETE FROM multiple_choices WHERE question_id = ?";
  $answer = "DELETE FROM correct_answers WHERE question_id = ?";

  $sql = "DELETE FROM questions WHERE id = ?";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param('i', $id);
  $stmt->execute();

  $stmt_multi = $mysqli->prepare($multi);
  $stmt_multi->bind_param('i', $id);
  $stmt_multi->execute();

  $stmt_answer = $mysqli->prepare($answer);
  $stmt_answer->bind_param('i', $id);
  $stmt_answer->execute();
  $mysqli->close();
  $_SESSION['success'] = "Item Removed!";
  echo "<script>window.location.href = 'index.php';</script>";

}


 ?>
