<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../../PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="../../PRACTICE/style.css">
    <link rel="stylesheet" href="style.css?v=3">
    <title>QUIZ</title>

    <?php
        if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
        } else {
            $pageno = 1;
        }
        $no_of_records_per_page = 1;
        $offset = ($pageno -1) * $no_of_records_per_page;
     ?>

  </head>
  <body>

    <div class="bg full-height">
      <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
    <div class="container mt-5">
<?php
    include_once '../../PRACTICE/6-1/config.php';

    $total_pages_sql = "SELECT COUNT(*) FROM questions INNER JOIN correct_answers ON questions.id = correct_answers.question_id INNER JOIN multiple_choices ON correct_answers.question_id = multiple_choices.question_id";

    $count = $mysqli->query($total_pages_sql);
    $total_rows =  $count->fetch_array()[0];
    $total_pages = ceil($total_rows / $no_of_records_per_page);

    $sql = "SELECT questions.id as question_id, questions.question, multiple_choices.choice_a, multiple_choices.choice_b, multiple_choices.choice_c, correct_answers.answers FROM questions INNER JOIN correct_answers ON questions.id = correct_answers.question_id INNER JOIN multiple_choices ON correct_answers.question_id = multiple_choices.question_id LIMIT $offset, $no_of_records_per_page";
    $previous_link = $pageno <= 1 ? "#" : "?pageno=". ($pageno - 1);
    $next_link = $pageno >= $total_pages ? "#" : "?pageno=".($pageno + 1);
    if ($result = $mysqli->query($sql)) {
      if ($result->num_rows > 0) {
        while ($row = $result->fetch_array()) {
          echo "<div class='row'>";
          echo "<div class='col-12'>";
          echo "<p class='fw-bold mt-5'>$row[question]</p>";
          echo "<div>";
          echo "<div class='row'>";
          echo "<div class='col-md-4'>";
          echo "<input type='radio' name='answer' selected id='a' value='$row[choice_a]'>";
          echo "<label for='a' class='box a w-100'>";
          echo "<div class='course'>";
          echo "<span class='circle'></span> ";
          echo "<span class='subject'>$row[choice_a]</span>";
          echo "</div>";
          echo "</label>";
          echo "</div>";
          echo "<div class='col-md-4'>";
          echo "<input type='radio' name='answer' id='b'  value='$row[choice_b]'>";
          echo "<label for='b' class='box b w-100'>";
          echo "<div class='course'>";
          echo "<span class='circle'></span>";
          echo "<span class='subject'> $row[choice_b] </span>";
          echo "</div>";
          echo "</label> ";
          echo "</div>";
          echo "<div class='col-md-4'>";
          echo "<input type='radio' name='answer' id='c'  value='$row[choice_c]'>";
          echo "<label for='c' class='box c w-100'>";
          echo "<div class='course'> ";
          echo "<span class='circle'></span> ";
          echo "<span class='subject'> $row[choice_c] </span>";
          echo "</div>";
          echo "</label> ";
          echo "</div>";
          echo "</div>";
          echo "</div>";
          echo "</div>";
          echo "<div class='col-12'>";
          echo "<div class=''>";
          echo "<a href='$previous_link' class='btn btn-danger px-4 py-2 fw-bold float-start' >PREVIOUS</a>";
          echo "<a href='$next_link' style='pointer-events: none;' class='btn btn-danger px-4 py-2 fw-bold float-end' id='next' data-id='$row[question_id]' onclick='store_quiz(this)'>NEXT</a>";
          echo "<a href='result.php' style='pointer-events: none;' class='d-none btn btn-danger px-4 py-2 fw-bold float-end' id='result' data-id='$row[question_id]' onclick='store_quiz(this)'>GET RESULT</a>";
          echo "</div>";
          echo "</div>";
          echo "</div>";
        }
      }
    }
    $mysqli->close();
?>
      </div>
      </div>
  </body>

  <script type="text/javascript">
    function store_quiz(id) {
      var id = $(id).data('id');
      var answer = $('input[name="answer"]:checked').val();
      var pageno = <?php echo $pageno; ?> ;
      localStorage.setItem(id, answer);
    }

    $('input[name="answer"]').click(function() {
      var pageno = <?php echo $pageno; ?> ;

        if (pageno == 10) {
          $("#result").removeClass("d-none");
          $('#result').css('pointer-events','');
          $("#next").addClass("d-none");
        } else {
          $('#next').css('pointer-events','');
        }


    });



  </script>


</html>
