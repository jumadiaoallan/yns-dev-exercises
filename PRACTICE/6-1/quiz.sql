-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql-server
-- Generation Time: Jan 21, 2022 at 07:52 AM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz`
--

-- --------------------------------------------------------

--
-- Table structure for table `correct_answers`
--

CREATE TABLE `correct_answers` (
  `id` int NOT NULL,
  `question_id` int NOT NULL,
  `answers` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `correct_answers`
--

INSERT INTO `correct_answers` (`id`, `question_id`, `answers`) VALUES
(1, 1, 'Pacific Ocean'),
(2, 2, 'A flow of electrons'),
(3, 3, 'FBI'),
(4, 4, 'Agoraphobia'),
(5, 5, 'Wind of Change'),
(6, 6, '1,200 km/h'),
(7, 7, 'To count the rings on the trunk'),
(8, 8, 'A caterpillar'),
(9, 9, '7'),
(10, 10, 'Jewellery');

-- --------------------------------------------------------

--
-- Table structure for table `multiple_choices`
--

CREATE TABLE `multiple_choices` (
  `id` int NOT NULL,
  `question_id` int NOT NULL,
  `choice_a` varchar(255) NOT NULL,
  `choice_b` varchar(255) NOT NULL,
  `choice_c` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `multiple_choices`
--

INSERT INTO `multiple_choices` (`id`, `question_id`, `choice_a`, `choice_b`, `choice_c`) VALUES
(1, 1, 'Pacific Ocean', 'Atlantic Ocean', 'Indian Ocean'),
(2, 2, 'A flow of water', 'A flow of air', 'A flow of electrons'),
(3, 3, 'FIFA', 'FBI', 'ASEAN'),
(4, 4, 'Agoraphobia', 'Aerophobia', 'Acrophobia'),
(5, 5, 'Stairway to Heaven', 'Wind of Change', 'Don’t Stop Me Now'),
(6, 6, '120 km/h', '1,200 km/h', '400 km/h'),
(7, 7, 'To measure the width of the tree', 'To count the rings on the trunk', 'To count the number of leaves'),
(8, 8, ' A moth', 'A butter', 'A caterpillar'),
(9, 9, '4', '7', '6'),
(10, 10, 'Rune stones', 'Jewellery', 'Wool');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int NOT NULL,
  `question` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`) VALUES
(1, 'In 1768, Captain James Cook set out to explore which ocean?'),
(2, 'What is actually electricity?'),
(3, 'Which of the following is not an international organisation?'),
(4, 'Which of the following disorders is the fear of being alone?'),
(5, 'Which of the following is a song by the German heavy metal band “Scorpions”?'),
(6, 'What is the speed of sound?'),
(7, 'Which is the easiest way to tell the age of many trees?'),
(8, 'What do we call a newly hatched butterfly?'),
(9, 'In total, how many novels were written by the Bronte sisters?'),
(10, 'Which did Viking people use as money?');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `correct_answers`
--
ALTER TABLE `correct_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `multiple_choices`
--
ALTER TABLE `multiple_choices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `correct_answers`
--
ALTER TABLE `correct_answers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `multiple_choices`
--
ALTER TABLE `multiple_choices`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
