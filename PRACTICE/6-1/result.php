<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link rel="stylesheet" href="style.css">
    <title>Quiz Result</title>

<?php
  include_once '../../PRACTICE/6-1/config.php';

  $sql = "SELECT * FROM correct_answers";

  $arry = [];
  if ($result = $mysqli->query($sql)) {
    if ($result->num_rows > 0) {
      $row = $result->fetch_array();
      while ($row = $result->fetch_array()) {
        $arr['question_id'] = $row['question_id'];
        $arr['answer'] = $row['answers'];
        array_push($arry, $arr);
      }
    }
  }
  $mysqli->close();

  $correct_answer = json_encode($arry);

 ?>


    <script type="text/javascript">
      let answer = Object.entries(localStorage);
      let correct_answer = <?php echo $correct_answer; ?>;
      var score = 0;
        for (var i = 0; i < correct_answer.length; i++) {
          var cor = correct_answer[i]['question_id'];
          for (var e = 0; e < answer.length; e++) {
            if (cor == answer[e][0]) {
                if (answer[e][1] == correct_answer[i]['answer']) {
                  score += 1;
                }
            }
          }
        }


    </script>


  </head>
  <body>
    <div class="container">
      <div class='row'>
      <div class='col-12'>
          <div class="d-flex justify-content-center">
            <h3>QUIZ RESULT</h3>
          </div>
          <div class="d-flex justify-content-center">
            <h4>Your Score: </h4>
          </div>
          <div class="d-flex justify-content-center">
            <span>Percentage: <span id="percent_score">100</span>%</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>Correct Answer: <span id="score">10</span></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span>Total Items: <span id="total_item">10</span></span>
          </div>
      </div>
      <div class='col-12'>
          <div class='d-flex justify-content-center'>
            <a href="/PRACTICE/6-1/"> <button class='btn btn-danger px-4 py-2 fw-bold'>BACK TO MAIN PAGE</button></a>
          </div>
      </div>
      </div>
    </div>
  </body>
  <script type="text/javascript">
    document.getElementById("score").innerHTML = score;
    document.getElementById("percent_score").innerHTML = (score/answer.length) * 100;
    document.getElementById("total_item").innerHTML = answer.length;

  </script>
</html>
