<!DOCTYPE html>
<html>
<head>
	<title>Calendar</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
	<script src="../PRACTICE/nav.js" charset="utf-8"></script>
	<link rel="stylesheet" href="../PRACTICE/style.css">
	<style type="text/css">

		table {
			background-color: #fff;
			font-size: 16px;
		}

		th {
			background-color: #ADD8E6;
			color: #000;
		}

		td.bg-yellow {
			background-color: #ffffe0;
		}

		td.bg-orange {
			background-color: #ffa500;
		}

		td.bg-green {
			background-color: #90ee90;
		}

		td.bg-white {
			background-color: #fff;
		}

		td.bg-blue {
			background-color: #add8e6;
		}

		.pre {
			color: #333;
			text-decoration: none;
      float: left;
		}

    .next {
      color: #333;
			text-decoration: none;
      float: right;
    }
	</style>
</head>

<body >
		<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/PRACTICE/6-3.php"); ?>
		<div class="container card mt-5 p-3 text-center">
			<?php
            isset($_GET['date']) ? $date = strtotime($_GET['date'] . '-01') :  $date = date(time());

            $prev = date('Y-m', mktime(0, 0, 0, date('m', $date)-1, date('d'), date('Y', $date)));
            $next = date('Y-m', mktime(0, 0, 0, date('m', $date)+1, date('d'), date('Y', $date)));

            $day = date('d', $date) ;
            $month = date('m', $date) ;
            $year = date('Y', $date) ;

            $first_day = mktime(0, 0, 0, $month, 1, $year) ;
            $title = date('F', $first_day) ;
            $day_of_week = date('D', $first_day) ;

            switch ($day_of_week) {
                case "Sun": $blank = 0; break;
                case "Mon": $blank = 1; break;
                case "Tue": $blank = 2; break;
                case "Wed": $blank = 3; break;
                case "Thu": $blank = 4; break;
                case "Fri": $blank = 5; break;
                case "Sat": $blank = 6; break;
            }

            $days_in_month = date('t', mktime(0, 0, 0, $month, 1, $year));
        ?>

			<div class="right">
				<table class="table table-bordered">
					<tr>
						<th colspan='7'><?php echo $title. " ".$year; ?>
						</th>
					</tr>
					<tr>
						<td>Sat</td>
						<td>Mon</td>
						<td>Tue</td>
						<td>Wed</td>
						<td>Thu</td>
						<td>Fri</td>
						<td>Sun</td>
					</tr>

					<?php
         $day_count = 1;

         echo "<tr>";

         while ($blank > 0) {
             echo "<td></td>";
             $blank = $blank-1;
             $day_count++;
         }

         $day_num = 1;

         while ($day_num <= $days_in_month) {
             if ($month == date('m') && $year == date('Y') && $day_num == date('d')) {
                 echo "<td style='background-color: #ADD8E6;'> $day_num</td>";
             } else {
                 echo "<td >$day_num</td>";
             }
             $day_num++;
             $day_count++;

             if ($day_count > 7) {
                 echo "</tr><tr>";
                 $day_count = 1;
             }
         }

         while ($day_count >1 && $day_count <=7) {
             echo "<td> </td>";
             $day_count++;
         }

         ?>
         <div class="row">
           <div class="col-12">
             <h3>Calendar</h3>
           </div>
         </div>
				</table>
        <div class="row">
          <div class="col-sm-12">
            <a class="btn btn-warning pre" href="6-2.php?date=<?php echo $prev;  ?>">Previous</a>
            <a class="btn btn-primary next" href="6-2.php?date=<?php echo $next;  ?>">Next</a>
          </div>
        </div>

			</div>



</body>

</html>
