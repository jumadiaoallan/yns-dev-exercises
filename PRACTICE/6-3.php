<nav class="navbar navbar-fixed-top navbar-expand-lg navbar-dark bg-primary mt-2">
 <div class="container-fluid">
 	 <a class="navbar-brand" href="#">New Employees Guide</a>
  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main_nav"  aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  <div class="collapse navbar-collapse" id="main_nav">

	<ul class="navbar-nav">
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">  Exercises  </a>
		    <ul class="dropdown-menu">
			  <li><a class="dropdown-item" href="#"> HTML and PHP &raquo; </a>
			  	 <ul class="submenu dropdown-menu">
				    <li><a class="dropdown-item" href="/HTML_AND_PHP/1-1.php">Exercise 1-1</a></li>
				    <li><a class="dropdown-item" href="/HTML_AND_PHP/1-2.php">Exercise 1-2</a></li>
				    <li><a class="dropdown-item" href="/HTML_AND_PHP/1-3.php">Exercise 1-3</a></li>
            <li><a class="dropdown-item" href="/HTML_AND_PHP/1-4.php">Exercise 1-4</a></li>
            <li><a class="dropdown-item" href="/HTML_AND_PHP/1-5.php">Exercise 1-5</a></li>
            <li><a class="dropdown-item" href="/HTML_AND_PHP/1-6_1-13/1-6(input).php">Exercise 1-6</a></li>
            <li><a class="dropdown-item" href="/HTML_AND_PHP/1-6_1-13/1-7.php">Exercise 1-7</a></li>
            <li><a class="dropdown-item" href="/HTML_AND_PHP/1-6_1-13/1-8.php">Exercise 1-8</a></li>
            <li><a class="dropdown-item" href="/HTML_AND_PHP/1-6_1-13/1-9.php">Exercise 1-9</a></li>
            <li><a class="dropdown-item" href="/HTML_AND_PHP/1-6_1-13/1-10.php">Exercise 1-10</a></li>
            <li><a class="dropdown-item" href="/HTML_AND_PHP/1-6_1-13/1-11.php">Exercise 1-11</a></li>
            <li><a class="dropdown-item" href="/HTML_AND_PHP/1-6_1-13/1-12.php">Exercise 1-12</a></li>
				    <li><a class="dropdown-item" href="/HTML_AND_PHP/1-6_1-13/1-13.php">Exercise 1-13</a></li>
				 </ul>
			  </li>
        <li><a class="dropdown-item" href="#"> JAVASCRIPT &raquo; </a>
			  	 <ul class="submenu dropdown-menu">
				    <li><a class="dropdown-item" href="/JAVASCRIPT/2-1.php">Exercise 2-1</a></li>
				    <li><a class="dropdown-item" href="/JAVASCRIPT/2-2.php">Exercise 2-2</a></li>
				    <li><a class="dropdown-item" href="/JAVASCRIPT/2-3.php">Exercise 2-3</a></li>
            <li><a class="dropdown-item" href="/JAVASCRIPT/2-4.php">Exercise 2-4</a></li>
            <li><a class="dropdown-item" href="/JAVASCRIPT/2-5.php">Exercise 2-5</a></li>
            <li><a class="dropdown-item" href="/JAVASCRIPT/2-6(input).php">Exercise 2-6</a></li>
            <li><a class="dropdown-item" href="/JAVASCRIPT/2-7.php">Exercise 2-7</a></li>
            <li><a class="dropdown-item" href="/JAVASCRIPT/2-8.php">Exercise 2-8</a></li>
            <li><a class="dropdown-item" href="/JAVASCRIPT/2-9.php">Exercise 2-9</a></li>
            <li><a class="dropdown-item" href="/JAVASCRIPT/2-10.php">Exercise 2-10</a></li>
            <li><a class="dropdown-item" href="/JAVASCRIPT/2-11.php">Exercise 2-11</a></li>
            <li><a class="dropdown-item" href="/JAVASCRIPT/2-12.php">Exercise 2-12</a></li>
            <li><a class="dropdown-item" href="/JAVASCRIPT/2-13.php">Exercise 2-13</a></li>
            <li><a class="dropdown-item" href="/JAVASCRIPT/2-14.php">Exercise 2-14</a></li>
				    <li><a class="dropdown-item" href="/JAVASCRIPT/2-15.php">Exercise 2-15</a></li>
				 </ul>
			  </li>
        <li><a class="dropdown-item" href="#"> DATABASE &raquo; </a>
			  	 <ul class="submenu dropdown-menu">
				    <li><a class="dropdown-item" href="/DATABASE/3-1.php">Exercise 3-1</a></li>
				    <li><a class="dropdown-item" href="/DATABASE/3-2.php">Exercise 3-2</a></li>
				    <li><a class="dropdown-item" href="/DATABASE/3-3.php">Exercise 3-3</a></li>
            <li><a class="dropdown-item" href="/DATABASE/3-4.php">Exercise 3-4</a></li>
            <li><a class="dropdown-item" href="/DATABASE/3-5/login.php">Exercise 3-5</a></li>
				 </ul>
			  </li>
        <li><a class="dropdown-item" href="#"> ADVANCE SQL &raquo; </a>
			  	 <ul class="submenu dropdown-menu">
				    <li><a class="dropdown-item" href="/ADVANCE_SQL/4-1.php">Exercise 4-1</a></li>
				    <li><a class="dropdown-item" href="/ADVANCE_SQL/4-2.php">Exercise 4-2</a></li>
				    <li><a class="dropdown-item" href="/ADVANCE_SQL/4-3.php">Exercise 4-3</a></li>
            <li><a class="dropdown-item" href="/ADVANCE_SQL/4-4.php">Exercise 4-4</a></li>
				 </ul>
			  </li>
        <li><a class="dropdown-item" href="#"> LINUX/DOCKER &raquo; </a>
			  	 <ul class="submenu dropdown-menu">
				    <li><a class="dropdown-item" href="/DOCKER/5-1.png">Exercise 5-1</a></li>
				    <li><a class="dropdown-item" href="#">Exercise 5-2 -> See Docker Folder</a></li>
				    <li><a class="dropdown-item" href="#">Exercise 5-3 -> This index</a></li>
				 </ul>
			  </li>
        <li><a class="dropdown-item" href="#"> PRACTICE SYSTEMS / PROGRAMS &raquo; </a>
			  	 <ul class="submenu dropdown-menu">
				    <li><a class="dropdown-item" href="/PRACTICE/6-1/">Exercise 6-1</a></li>
				    <li><a class="dropdown-item" href="/PRACTICE/6-2.php">Exercise 6-2</a></li>
				    <li><a class="dropdown-item" href="/PRACTICE/6-3.php">Exercise 6-3 -> This Nav Bar</a></li>
				 </ul>
			  </li>
		    </ul>
		</li>
	</ul>
  </div>
 </div>
</nav>
