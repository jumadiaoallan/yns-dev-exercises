<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="PRACTICE/nav.js" charset="utf-8"></script>
    <link rel="stylesheet" href="PRACTICE/style.css">
    <title>Exercises</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
  </head>
  <body>
    <div class="mt-2">
      <?php include_once('PRACTICE/6-3.php'); ?>
    </div>

    <div class="container mt-3">
      <div class="text-center">
        <h3 class="mt-2">Exercises</h3>
      </div>
      <table class="table table-bordered">
        <tr>
          <th colspan="3" class="text-center">HTML and PHP</th>
        </tr>
        <tr>
          <th>No.</th>
          <th>TITLE</th>
          <th>DEMO</th>
        </tr>
        <tr>
          <td>1.1</td>
          <td>Show Hello World.</td>
          <td> <a href="HTML_AND_PHP/1-1.php">1-1.php</a> </td>
        </tr>
        <tr>
          <td>1.2</td>
          <td>The four basic operations of arithmetic.</td>
          <td> <a href="HTML_AND_PHP/1-2.php">1-2.php</a> </td>
        </tr>
        <tr>
          <td>1.3</td>
          <td>Show the greatest common divisor.</td>
          <td> <a href="HTML_AND_PHP/1-3.php">1-3.php</a> </td>
        </tr>
        <tr>
          <td>1.4</td>
          <td>Solve the FizzBuzz problem.</td>
          <td> <a href="HTML_AND_PHP/1-4.php">1-4.php</a> </td>
        </tr>
        <tr>
          <td>1.5</td>
          <td>Input date. Then show 3 days from the input date and the days of the week of each date.</td>
          <td> <a href="HTML_AND_PHP/1-5.php">1-5.php</a> </td>
        </tr>
        <tr>
          <td>1.6</td>
          <td>Input user information. Then show it on the next page.</td>
          <td> <a href="HTML_AND_PHP/1-6_1-13/1-6(input).php">1-6.php</a> </td>
        </tr>
        <tr>
          <td>1.7</td>
          <td>Add validation in the user information form(required, numeric, character, mailaddress).</td>
          <td> <a href="HTML_AND_PHP/1-6_1-13/1-7.php">1-7.php</a> </td>
        </tr>
        <tr>
          <td>1.8</td>
          <td>Store inputted user information into a CSV file.</td>
          <td> <a href="HTML_AND_PHP/1-6_1-13/1-8.php">1-8.php</a> </td>
        </tr>
        <tr>
          <td>1.9</td>
          <td>Show the user information using table tags.</td>
          <td><a href="HTML_AND_PHP/1-6_1-13/1-9.php">1-9.php</a></td>
        </tr>
        <tr>
          <td>1.10</td>
          <td>Upload images.</td>
          <td><a href="HTML_AND_PHP/1-6_1-13/1-10.php">1-10.php</a></td>
        </tr>
        <tr>
          <td>1.11</td>
          <td>Show uploaded images in the table</td>
          <td><a href="HTML_AND_PHP/1-6_1-13/1-11.php">1-11.php</a></td>
        </tr>
        <tr>
          <td>1.12</td>
          <td>Add pagination in the list page.</td>
          <td><a href="HTML_AND_PHP/1-6_1-13/1-12.php">1-12.php</a></td>
        </tr>
        <tr>
          <td>1.13</td>
          <td>Create a login form and embed it into the system that you developed.</td>
          <td><a href="HTML_AND_PHP/1-6_1-13/1-13.php">1-13.php</a></td>
        </tr>
      </table>

      <table class="table table-bordered mt-3">
        <tr>
          <th colspan="3" class="text-center">JAVASCRIPT</th>
        </tr>
        <tr>
          <th>No.</th>
          <th>TITLE</th>
          <th>DEMO</th>
        </tr>
        <tr>
          <td>2.1</td>
          <td>Show alert.</td>
          <td><a href="JAVASCRIPT/2-1.php">2-1.php</a></td>
        </tr>
        <tr>
          <td>2.2</td>
          <td>Confirm dialog and redirection</td>
          <td><a href="JAVASCRIPT/2-2.php">2-2.php</a></td>
        </tr>
        <tr>
          <td>2.3</td>
          <td>The four basic operations of arithmetic</td>
          <td><a href="JAVASCRIPT/2-3.php">2-3.php</a></td>
        </tr>
        <tr>
          <td>2.4</td>
          <td>Show prime numbers</td>
          <td><a href="JAVASCRIPT/2-4.php">2-4.php</a></td>
        </tr>
        <tr>
          <td>2.5</td>
          <td>Input characters in the textbox and show it in the label.</td>
          <td><a href="JAVASCRIPT/2-5.php">2-5.php</a></td>
        </tr>
        <tr>
          <td>2.6</td>
          <td>Press the button and add a label below the button.</td>
          <td><a href="JAVASCRIPT/2-6.php">2-6.php</a></td>
        </tr>
        <tr>
          <td>2.7</td>
          <td>Show alert when you click an image.</td>
          <td><a href="JAVASCRIPT/2-7.php">2-7.php</a></td>
        </tr>
        <tr>
          <td>2.8</td>
          <td>Show alert when you click a link.</td>
          <td><a href="JAVASCRIPT/2-8.php">2-8.php</a></td>
        </tr>
        <tr>
          <td>2.9</td>
          <td>Change text and background color when you press buttons.</td>
          <td><a href="JAVASCRIPT/2-9.php">2-9.php</a></td>
        </tr>
        <tr>
          <td>2.10</td>
          <td>Scroll screen when you press buttons.</td>
          <td><a href="JAVASCRIPT/2-10.php">2-10.php</a></td>
        </tr>
        <tr>
          <td>2.11</td>
          <td>Change background color using animation.</td>
          <td><a href="JAVASCRIPT/2-11.php">2-11.php</a></td>
        </tr>
        <tr>
          <td>2.12</td>
          <td>Show another image when you mouse over an image. Then show the original image when you mouse out.</td>
          <td><a href="JAVASCRIPT/2-12.php">2-12.php</a></td>
        </tr>
        <tr>
          <td>2.13</td>
          <td>Change the size of images when you press buttons.</td>
          <td><a href="JAVASCRIPT/2-13.php">2-13.php</a></td>
        </tr>
        <tr>
          <td>2.14</td>
          <td>Show images according to the options in the combo box</td>
          <td><a href="JAVASCRIPT/2-14.php">2-14.php</a></td>
        </tr>
        <tr>
          <td>2.15</td>
          <td>Show current date and time in real time.</td>
          <td><a href="JAVASCRIPT/2-15.php">2-15.php</a></td>
        </tr>
      </table>

      <table class="table table-bordered">
        <tr>
          <th colspan="3" class="text-center">DATABASE</th>
        </tr>
        <tr>
          <th>No.</th>
          <th>TITLE</th>
          <th>DEMO</th>
        </tr>
        <tr>
          <td>3.1</td>
          <td>Install phpMyAdmin</td>
          <td><a href="DATABASE/3-1.php">3-1.php</a></td>
        </tr>
        <tr>
          <td>3.2</td>
          <td>CREATE TABLE</td>
          <td><a href="DATABASE/3-2.php">3-2.php</a></td>
        </tr>
        <tr>
          <td>3.3</td>
          <td>INSERT, UPDATE, DELETE</td>
          <td><a href="DATABASE/3-3.php">3-3.php</a></td>
        </tr>
        <tr>
          <td>3.4</td>
          <td>Solve problems using SQL</td>
          <td><a href="DATABASE/3-4.php">3-4.php</a></td>
        </tr>
        <tr>
          <td>3.5</td>
          <td>Use the database in the applications that you developed.</td>
          <td><a href="DATABASE/3-5/login.php">3-5.php</a></td>
        </tr>
      </table>

      <table class="table table-bordered">
        <tr>
          <th colspan="4" class="text-center">ADVANCE SQL</th>
        </tr>
        <tr>
          <th>No.</th>
          <th>TITLE</th>
          <th>Descriptions</th>
          <th>DEMO</th>
        </tr>
        <tr>
          <td>4.1</td>
          <td>Selecting by age using birth date</td>
          <td> <p>Using same data as SQL problems. Get employees who is older than 30 years old but younger than 40 years old. (Result may vary based on current date)</p> </td>
          <td><a href="ADVANCE_SQL/4-1.php">4-1.php</a></td>
        </tr>
        <tr>
          <td>4.2</td>
          <td>List all therapists according to their daily work shifts</td>
          <td>
            <p>For this exercise you need to create data from given scenario.</p>
            <p>There are 5 therapists name John, Arnold, Robert, Ervin and Smith who are working in a Massage Company with a schedule from 6am in the morning up to 05:59am in the next morning.</p>
            <ul>
              <li>A. First you need to create a table for therapists (id, name) and create a table name for their daily_work_shifts (id, therapist_id, target_date, start_time, end_time)</li>
              <li>B. Insert daily_work_shifts schedule today where: </li>
              <ul>
                <li>1. John works from 2pm today and finish his duty on body massage after 60mins</li>
                <li>2. Arnold works from 10pm and finish his spa duty on 11pm today</li>
                <li>3. Robert works from 12 midnight on today and finish neck massage after 60mins</li>
                <li>4. Ervin works from 5am on today and finish body massage after 30mins</li>
                <li>5. John continue his duty from 9pm and finish his duty on another body massage after 45mins</li>
                <li>6. Smith works from 5:30am on today and finish on body scrub after 20mins</li>
                <li>7. Robert continue his duty from 2am and finish his spa duty on 2:30am </li>
              </ul>
              <li>C. Select mst_therapist_id, target_date, start_time, end_time and sort_start_time from daily_work_shifts using inner join to therapists order by target_date ASC and sort_start_time ASC</li>
            </ul>
            <p>Use mysql case function where daily_work_shifts start_time is less than or equal to 05:59:59 and start_time is greater than or equal to 00:00:00 (12midnight) sort_start_time will be equal to (target date +1day start_time) example: 2018-10-10 01:00:00 else sort_start_time will be target_date start_time.</p>

           </td>
          <td><a href="ADVANCE_SQL/4-2.php">4-2.php</a></td>
        </tr>
        <tr>
          <td>4.3</td>
          <td>Using Case conditional statement</td>
          <td>
            <p>Using same data as SQL problems. Get employees information with full position name.</p>
            <ul>
              <li>CEO =  Chief Executive Officer</li>
              <li>CTO = Chief Technical Officer</li>
              <li>CFO = Chief Financial Officer</li>
              <li>Other position will remain the same</li>
            </ul>
          </td>
          <td><a href="ADVANCE_SQL/4-3.php">4-3.php</a></td>
        </tr>
        <tr>
          <td>4.4</td>
          <td>Create sql that will display the OUTPUT</td>
          <td>
            <ul>
              <li>Table master data</li>
              <ul>
                <li> <img src="ADVANCE_SQL/master_data.png" alt=""> </li>
              </ul>
              <li>OUTPUT</li>
              <ul>
                <li><img src="ADVANCE_SQL/output.png" alt=""></li>
              </ul>
            </ul>
          </td>
          <td><a href="ADVANCE_SQL/4-4.php">4-4.php</a></td>
        </tr>
      </table>

      <table class="table table-bordered">
        <tr>
          <th colspan="3" class="text-center">Linux / Docker</th>
        </tr>
        <tr>
          <th>No.</th>
          <th>TITLE</th>
          <th>DEMO</th>
        </tr>
        <tr>
          <td>5.1</td>
          <td>Install docker desktop</td>
          <td> <a href="DOCKER/5-1.png">5-1.php</a> </td>
        </tr>
        <tr>
          <td>5.2</td>
          <td>Create Dockerfile and docker-compose.yml</td>
          <td> Please see Docker folder </td>
        </tr>
        <tr>
          <td>5.3</td>
          <td>Clone the exercise repository inside the web-server.</td>
          <td> This current exercises </td>
        </tr>

      </table>

      <table class="table table-bordered mt-2">
        <tr>
          <th colspan="3" class="text-center">PRACTICE/PROGRAM</th>
        </tr>
        <tr>
          <th>No.</th>
          <th>TITLE</th>
          <th>DEMO</th>
        </tr>
        <tr>
          <td>6.1</td>
          <td>Quiz with three multiple choices</td>
          <td> <a href="PRACTICE/6-1/">6-1.php</a> </td>
        </tr>
        <tr>
          <td>6.2</td>
          <td>Calendar</td>
          <td> <a href="PRACTICE/6-2.php">6-2.php</a> </td>
        </tr>
        <tr>
          <td>6.3</td>
          <td>Navigation</td>
          <td> <a href="PRACTICE/6-3.php">6-3.php</a> </td>
        </tr>
      </table>

    </div>




  </body>
</html>
